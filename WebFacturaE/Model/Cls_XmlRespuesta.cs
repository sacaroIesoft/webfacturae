﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFacturaE.Model
{
    public class Cls_XmlRespuesta
    {
        public string claveNumerica { get; set; }
        public string consecutivo   { get; set; }
        public string estado        { get; set; }
        public string mensaje       { get; set; }
    }
}