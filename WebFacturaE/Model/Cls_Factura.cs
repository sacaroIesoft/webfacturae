﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFacturaE.Model
{
    public class Cls_Factura
    {
        public Cls_Encabezado    Encabezado  { get; set; }
        public List<Cls_Detalle> Detalle     { get; set; }

        public Cls_Factura()
        {
            Detalle = new List<Cls_Detalle>();
        }
    }
    public class Cls_Encabezado
    {
        public DateTime FechaFactura    { get; set; }
        public string   NumeroFactura   { get; set; }
        public string   TipoDoc         { get; set; }
        public string   TipoFac         { get; set; }
        public string   IDCliente       { get; set; }
        public string   NombreCliente   { get; set; }
        public string   CorreoCliente   { get; set; }
        public string   Moneda          { get; set; }
        public string   Otros           { get; set; }
        public int      NumTelfCliente  { get; set; }
        public int      Sucursal        { get; set; }
        public int      Terminal        { get; set; }

    }
    public class Cls_Detalle
    {
        public char      Tipo           { get; set; }//M=Mercancia   S=servicio
        public string    CodProducto    { get; set; }
        public string    Descripcion    { get; set; }
        public decimal   Cantidad        { get; set; }
        public decimal   PrecioUnitario { get; set; }
        public decimal   MontoDescuento { get; set; }
        public decimal   MontoImpuesto  { get; set; }
    }
}