﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFacturaE.Model
{
    public class Cls_ListadoFact
    {
        public string Fecha         { get; set; }
        public string Consecutivo   { get; set; }
        public string Estado        { get; set; }
    }
}