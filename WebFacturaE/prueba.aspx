﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="prueba.aspx.cs" Inherits="WebFacturaE.prueba" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="Content/bootstrap.min.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title></title>
</head>
<body>
    <div class="row">
    <form id="form1" runat="server">
        <div class=" col-lg-3">
            </div>
        <div class="col-lg-6 table-responsive">
            <asp:Button class="btn btn-primary btn-lg" ID="Button1" runat="server" Text="Crear xml" OnClick="Button1_Click"  />
            <br />
            <br />
        
        <asp:GridView ID="GridView1"  CssClass="table" Width="100%" runat="server" CellPadding="8" CellSpacing="5" ForeColor="#333333" 
            GridLines="None" PageSize="4" EmptyDataText="No hay datos" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" BorderWidth="1px" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>

        </div>
        <div class="col-lg-3">
            </div>

    </form>
        </div>
         <div class="row">
            <div class="alert alert-danger alert-dismissable col-lg-12">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <p id="Msj" runat="server"></p>
            </div>
        </div>
</body>
</html>
