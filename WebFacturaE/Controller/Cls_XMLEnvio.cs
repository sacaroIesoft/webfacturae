﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WebFacturaE.Model;

namespace WebFacturaE.Controller
{
    public class Cls_XMLEnvio
    {
        public string GenerarXML(Cls_Factura cls_Factura)
        {
            
            XDocument xDocument = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement("root",

                    new XElement("Encabezado",
                        new XElement("NumeroFactura",cls_Factura.Encabezado.NumeroFactura),
                        new XElement("FechaFactura", cls_Factura.Encabezado.FechaFactura.ToString("yyyy-MM-dd'T'HH:mm:ssZ")),
                        new XElement("TipoDoc", cls_Factura.Encabezado.TipoDoc),
                        new XElement("TipoFac", cls_Factura.Encabezado.TipoFac),
                        new XElement("Sucursal",cls_Factura.Encabezado.Sucursal),
                        new XElement("Terminal", cls_Factura.Encabezado.Terminal),
                        new XElement("IDCliente", cls_Factura.Encabezado.IDCliente),
                        new XElement("NombreCliente", cls_Factura.Encabezado.NombreCliente),
                        new XElement("NumTelfCliente",  cls_Factura.Encabezado.NumTelfCliente),
                        new XElement("CorreoCliente", cls_Factura.Encabezado.CorreoCliente),
                        new XElement("Moneda",  cls_Factura.Encabezado.Moneda),
                        new XElement("Otros",  cls_Factura.Encabezado.Otros)
                    ),
                    new XElement("Detalle",
                          from linea in cls_Factura.Detalle
                          select new XElement("Linea",
                                new XElement("Tipo",linea.Tipo),
                                new XElement("CodProducto", linea.CodProducto),
                                new XElement("Cantidad", linea.Cantidad),
                                new XElement("Descripcion", linea.Descripcion),
                                new XElement("PrecioUnitario",MontoToString( linea.PrecioUnitario)),
                                new XElement("MontoDescuento",MontoToString( linea.MontoDescuento)),
                                new XElement("MontoImpuesto",MontoToString( linea.MontoImpuesto))
                           )

                    )
                )
              );
           
           return xDocument.ToString();
        }

        private string MontoToString( decimal dcm_Monto)
        {
            string str_result= decimal.Round((dcm_Monto), 5).ToString();
            str_result = str_result.Replace(",", ".");
            return str_result;
        }
    }
}