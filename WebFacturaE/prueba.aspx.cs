﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Http;
using WebFacturaE.Model;
using WebFacturaE.Controller;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;

namespace WebFacturaE
{
    public partial class prueba : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LLenarGrid();
        }

        private Cls_Factura cargarDatos()
        {
            Cls_Factura cls_Factura = new Cls_Factura();
            Cls_Encabezado cls_Encabezado = new Cls_Encabezado
            {
                CorreoCliente = "info@Iesoft.net",
                FechaFactura = DateTime.Now,
                IDCliente = "101110111",
                Moneda = "CRC",
                NombreCliente = "Iesoft",
                NumeroFactura = "001",
                NumTelfCliente = 23331144,
                Otros = "otros",
                Sucursal = 2,
                Terminal = 1,
                TipoDoc = "Factura",
                TipoFac = "Contado"
            };
            Cls_Detalle cls_Detalle = new Cls_Detalle
            {
                Cantidad=1,
                CodProducto="cod_1",
                Descripcion="Producto 1",
                MontoDescuento=0,
                PrecioUnitario=4000,
                MontoImpuesto=520,
                Tipo='M'
            };
            Cls_Detalle cls_Detalle2 = new Cls_Detalle
            {
                Cantidad = 2,
                CodProducto = "cod_2",
                Descripcion = "Producto 2",
                MontoDescuento = 0.00M,
                PrecioUnitario = 2000.00M,
                MontoImpuesto = 520.00M,
                Tipo = 'M'
            };
            cls_Factura.Encabezado = cls_Encabezado;
            cls_Factura.Detalle.Add( cls_Detalle);
            cls_Factura.Detalle.Add(cls_Detalle2);
            return cls_Factura;
        }

        private void LLenarGrid()
        {
            List<Cls_ListadoFact> lst_listadoFact = new List<Cls_ListadoFact>
           {
               new Cls_ListadoFact
               {
                   Fecha="2018/01/01",
                   Consecutivo="00000001",
                   Estado="Pendiente"
               },
                new Cls_ListadoFact
               {
                   Fecha="2018/01/02",
                   Consecutivo="00000002",
                   Estado="Pendiente"
               },
                new Cls_ListadoFact
               {
                   Fecha="2018/01/03",
                   Consecutivo="00000003",
                   Estado="Procesando"
               },
                new Cls_ListadoFact
               {
                   Fecha="2018/01/04",
                   Consecutivo="00000001",
                   Estado="Pendiente"
               },
                new Cls_ListadoFact
               {
                   Fecha="2018/01/05",
                   Consecutivo="00000002",
                   Estado="Error"
               },
                new Cls_ListadoFact
               {
                   Fecha="2018/01/06",
                   Consecutivo="00000003",
                   Estado="aceptado"
               }
           };
            GridView1.DataSource = lst_listadoFact;
            GridView1.DataBind();
        }

        private async  void Enviar(string str_Xml)
        { try
            {
            string str_URL = "http://localhost:54547/api/" + "FacturaIesoft/EnviarFacturaHacienda/info@iesoft.net/123456/";
            HttpContent httpcontent = new StringContent(str_Xml, Encoding.UTF8, "text/xml");
            HttpClient client = new HttpClient();
            HttpResponseMessage responseMsj=   await client.PostAsync(str_URL, httpcontent);
            string str_Content = responseMsj.Content.ReadAsStringAsync().Result;

                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.LoadXml( str_Content);
                string str_estado = xmlDoc.SelectNodes(@"/root/estado")[0].InnerText;
                string str_Msj= xmlDoc.SelectNodes(@"/root/mensaje")[0].InnerText;
                Msj.InnerText = "Estado: " + str_estado + "\n Mensaje: " + str_Msj;

            }catch(System.Threading.Tasks.TaskCanceledException)
            {
                //Servidor no responde (api)
            }
            catch (Exception ex)
            {
                string s = ex.Message;
                ex.ToString();
            }

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Cls_XMLEnvio cls_XML = new Cls_XMLEnvio();
                string str_xml = cls_XML.GenerarXML(cargarDatos());

                Enviar(str_xml);

            }
            catch (Exception ex)
            {
               string msj= ex.Message;
                ex.ToString();
            }
        }


        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            LLenarGrid();
        }
    }
}